const API_KEY = 'api_key=57965453134b9fc0a963358b324afc94';
const BASE_URL = 'https://api.themoviedb.org/3';

export const constants = {
   SEARCH_URL: BASE_URL + '/search/movie?' + API_KEY,
   POPULAR_URL: BASE_URL + '/movie/popular?' + API_KEY,
   TOPRATED_URL: BASE_URL + '/movie/top_rated?' + API_KEY,
   UPCOMING_URL: BASE_URL + '/movie/upcoming?' + API_KEY,
   DETAILS_URL: BASE_URL + '/movie/{movie_id}?' + API_KEY,
   IMAGE_URL: 'https://image.tmdb.org/t/p/w500'
}
