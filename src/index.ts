import { constants } from "./constants/constants";
import { getMovies } from "./getMovies";
import { popularMoive } from "./selection-films/popularMovie";
import { searchMovie } from "./selection-films/searchMovie";
import { topRatedMovie } from "./selection-films/topRatedMovie";
import { upcommingMovie } from "./selection-films/upcomingMovie";
import { paginationFunc } from "./pagination/pagination";
import { favouriteMoive } from "./favourite-films/favouriteFilms";

export async function render(): Promise<void> {
    // TODO render your app here
    // interface IMovie {
    // }

    getMovies(constants.POPULAR_URL);
    popularMoive();
    searchMovie();
    topRatedMovie();
    upcommingMovie();
    paginationFunc();
    favouriteMoive();
}
