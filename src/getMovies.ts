import { constants } from "./constants/constants";
import { paginationConst } from "./pagination/paginationConst";
const cardSection = <HTMLElement>document.getElementById('film-container');
const random_movie = <HTMLElement>document.getElementById('random-movie');

export function getMovies(url: string) {
    fetch(url)
        .then(res => res.json())
        .then((data) => {
            if (data.results.length !== 0) {
                for (let i = 0; i < data.results.length; i++) {
                    localStorage.setItem('id', data.results[i].id)
                    localStorage.getItem('id');
                }
                showMovies(data.results);
                showRandomMovies(data.results);
                paginationConst.currentPage = data.page;
                paginationConst.nextPage = paginationConst.currentPage + 1;
            }
        })
        .catch(err => console.log(err))
}


function showMovies(data: Array<string>) {
    data.forEach((card: any) => {
        const { poster_path, overview, release_date } = card;
        const movieEL = document.getElementById('movie');
        movieEL!.innerHTML = ` 
           <div  class="card shadow-sm">
           <img src="${constants.IMAGE_URL + poster_path}" />
           <svg xmlns="http://www.w3.org/2000/svg" stroke="red" fill="#ff000078" width="50" height="50"
               class="bi bi-heart-fill position-absolute p-2" viewBox="0 -2 18 22">
               <path fill-rule="evenodd"
                   d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" />
           </svg>
           <div class="card-body">
               <p class="card-text truncate">
                 ${overview}
               </p>
               <div class="
                           d-flex
                           justify-content-between
                           align-items-center
                       ">
                   <small class="text-muted">${release_date}</small>
               </div>
           </div>
       </div>
               `;

        cardSection.appendChild(movieEL as HTMLElement);
    })
}

function showRandomMovies(data: Array<string>) {
    data.forEach((randomMovie: any) => {
        const { title, overview, backdrop_path } = randomMovie;
        const randomMovieEL = document.getElementById('random-movie-element');
        randomMovieEL!.innerHTML = ` 
        <img src="${constants.IMAGE_URL + backdrop_path}" />
        <div class="row py-lg-5">
        <div class="col-lg-6 col-md-8 mx-auto" style="background-color: #2525254f">
            <h1 id="random-movie-name" class="fw-light text-light">${title}</h1>
            <p id="random-movie-description" class="lead text-white">
                ${overview}
            </p>
        </div>
    </div>
               `;

        random_movie.appendChild(randomMovieEL as HTMLElement);
    })
}

