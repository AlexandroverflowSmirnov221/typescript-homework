import { constants } from "../constants/constants";
import { getMovies } from "../getMovies";
const submit = <HTMLElement>document.getElementById('submit');
const search = <HTMLInputElement>document.getElementById('search');
const form = <HTMLInputElement>document.getElementById('form');


export function searchMovie() {
   submit!.addEventListener('click', (e) => {
      const searchTerm = search.value;

      if (searchTerm) {
         getMovies(constants.SEARCH_URL + '&query=' + searchTerm);
      }
   })
   form!.addEventListener('submit', (e) => {
      const searchTerm = search.value;
      if (searchTerm) {
         getMovies(constants.SEARCH_URL + '&query=' + searchTerm);
      }
   })

}