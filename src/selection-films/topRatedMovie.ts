import { constants } from "../constants/constants";
import { getMovies } from "../getMovies";
const top_rated = <HTMLElement>document.getElementById('top_rated');

export function topRatedMovie() {
   top_rated!.addEventListener('click', (e) => {
      getMovies(constants.TOPRATED_URL);
   })

}
