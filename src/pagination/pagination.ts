import { constants } from "../constants/constants";
import { getMovies } from "../getMovies";
import { paginationConst } from "./paginationConst";
const pagination = <HTMLElement>document.getElementById('load-more');

export function paginationFunc() {
   
   pagination!.addEventListener('click', (e) => {
      pageCall(paginationConst.nextPage);
   })
}

function pageCall(page: number) {
   let url = constants.POPULAR_URL + '&page=' + page;
   getMovies(url);
}