# MOVIE APP

## API KEY

api_key=57965453134b9fc0a963358b324afc94

## Instalation

`npm install`

## Run project

`npm run start`

Open http://localhost:9090/ to view it in the browser.

## Check typescript and linter errors

`npm run lint`
